###############################################################################
#Author: Hippolyte Signargout
#Last update: August 14, 2019
#This file defines functions which can be used to analyse the output of a
# relaxed SCOPF otpimisation.
###############################################################################

include("model_builds.jl")

"""Separates contingencies into sets in which control variables are equal
result -> result of a relaxed scopf optimization
error -> accepted error for equality"""
function separate(result::Dict, error::Number)
    nets=result["solution"]["nw"]
    values=Dict()
    for k in keys(nets)
        compare!(values,k,nets, error)
    end
    return values
end

""" Compares a contingency with the sets and adds it where it belongs
values -> sets of contingencies, gets filled within the function
key -> contingency being added to values
nets -> solutions of the relaxed SCOPF for each contingency"""
function compare!(values::Dict, key::String, nets::Dict, error::Number)
    for (k,e) in values
        test= eq_dict(nets[key]["gen"], e["gen"], error)
        if test
            e["amount"]+=1
            append!(e["list"],[key])
            return
        end
    end
    values[key]=Dict("gen"=>nets[key]["gen"],"amount"=>1,"list"=>[key])
    return
end

"""Finds the biggest set"""
function acceptable_contingencies(result::Dict,error::Number)
    values=separate(result,error)
    amount=0
    key=""
    for (k,entry) in values
        if entry["amount"]>amount
            key = k
            amount = entry["amount"]
        end
    end
    return values[key]["list"]
end

"""Comparing leaves that are not numbers"""
function eq_dict(a,b, error::Number)
    return isequal(a,b)
end

"""Comparing dictionary and leaf"""
function eq_dict(a,dict::AbstractDict, error::Number)
    return false
end

"""Comparing dictionary and leaf"""
function eq_dict(dict::AbstractDict,b, error::Number)
    return false
end

"""Comparing dictionaries"""
function eq_dict(dict1::AbstractDict,dict2::AbstractDict, error::Number)
    keys1=keys(dict1)
    if keys1!=keys(dict2)
        return false
    else
        for key in keys1
            if !(eq_dict(dict1[key],dict2[key], error))
                return false
            end
        end
    end
    return true #If no false has been returned the dictionaries are equal
end

"""Comparing number leaves"""
function eq_dict(x::Number,y::Number, error::Number)
    if isequal(x,NaN)&&isequal(y,NaN)   #In the DC case, qg==Nan
        return true
    else
        return abs(x-y)<error
    end
end
