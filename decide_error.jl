###############################################################################
#Author: Hippolyte Signargout
#Last update: August 14, 2019
#This file has been used to gather the data in the graphs in the dissertation.
# 'rate' needs to be defined before including this file for the CSCOPF models.
###############################################################################

include("rel_analysis.jl")
networks=["case5.m", "case14.m"]
ret=Dict()

#The following error arrays have been used to get some results
errors=[i*(10.0^(-3)) for i in 1:10]
#errors=[10.0^(-i) for i in 0:10]
#errors=[i/10 for i in 1:10]
for file in networks
    ret[file]=[[],[],[]]
    #Solve the relaxed SCOPF
    rpmdc = build_dc_rel(file)
    rpmac = build_ac_rel(file)
    network_data=parse_file("data/$(file)")
    n=length(network_data["branch"])+1
    relresdc=optimize_model!(rpmdc,with_optimizer(Ipopt.Optimizer))
    relresac=optimize_model!(rpmac,with_optimizer(Ipopt.Optimizer))
    #Compare different error thresholds
    for error in errors
        ac=acceptable_contingencies(relresac, error)
        dc=acceptable_contingencies(relresdc, error)
        temp=[]
        aesult=(optimize_model!(build_ac_cor(file,ac),with_optimizer(Ipopt.Optimizer)))["termination_status"]
        besult=""
        if aesult==relresdc["termination_status"]
            besult="s"
        else
            besult="u"
        end
        dcc=length(dc)
        acc=length(ac)
        append!(ret[file][1],100*dcc/n) #Percentage of contingencies accepted (DC)
        append!(ret[file][2],100*acc/n) #Percentage of contingencies accepted (AC)
        append!(ret[file][3],besult)    #Is the problem 's'olvable or 'u'nfeasible
    end                                 # with these contingencies?
end
#Print result
for kv in ret
    println(kv)
end
