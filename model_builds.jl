###############################################################################
#Author: Hippolyte Signargout
#Last update: August 14, 2019
#This file consists of functions meant to be arguments of the 'build_model'
# method for various versions of the SCOPF, and functions used to create a model
# for these versions from a .m file.
###############################################################################

using PowerModels
using Ipopt
using JuMP

"""Preventive only SCOPF
ONLY FOR DC FRAMEWORK"""
function post_mndc_scopf(pm::GenericPowerModel)
    pg=Dict()
    pcon=length(nws(pm))  #'pcon' corresponds to the pre-contingency case
    for (n, network) in nws(pm)
        variable_voltage(pm, nw=n)
        variable_generation(pm, nw=n)
        variable_branch_flow(pm, nw=n)
        variable_dcline_flow(pm, nw=n)

        for i in ids(pm, :gen, nw=n)                    #Give a name
            pg["$(n)_$(i)"]=var(pm, n, pm.ccnd, :pg, i) #to the variables
        end                                             #for the constraints

        constraint_model_voltage(pm, nw=n)

        for i in ids(pm, :ref_buses, nw=n)
            constraint_theta_ref(pm, i, nw=n)
        end

        for i in ids(pm, :bus, nw=n)
            constraint_power_balance_shunt(pm, i, nw=n)
        end

        for i in ids(pm, :branch, nw=n)
            constraint_ohms_yt_from(pm, i, nw=n)
            constraint_ohms_yt_to(pm, i, nw=n)
            constraint_voltage_angle_difference(pm, i, nw=n)
            constraint_thermal_limit_from(pm, i, nw=n)
            constraint_thermal_limit_to(pm, i, nw=n)
        end

        for i in ids(pm, :dcline, nw=n)
            constraint_dcline(pm, i, nw=n)
        end
    end

    for (n, network) in nws(pm)                                                         #New loop is so that all variables are created before adding constraints
        if pcon!=n                                                                      #For each post-contingency network
            for i in ids(pm, :gen, nw=n)                                                #And each bus on the network
                JuMP.@constraint(pm.model, pg["$(pcon)_$(i)"]==pg["$(n)_$(i)"])        #Force equality in generation with precontingency
            end                                                                         #
        end                                                                             #
    end                                                                                 #
    objective_min_fuel_and_flow_cost(pm)
end

"""Preventive only SCOPF
ONLY FOR DC FRAMEWORK"""
function post_mnac_scopf(pm::GenericPowerModel)
    pg,qg=Dict(),Dict()
    pcon=length(nws(pm))  #'pcon' corresponds to the pre-contingency case
    for (n, network) in nws(pm)
        variable_voltage(pm, nw=n)
        variable_generation(pm, nw=n)
        variable_branch_flow(pm, nw=n)
        variable_dcline_flow(pm, nw=n)

        for i in ids(pm, :gen, nw=n)
            pg["$(n)_$(i)"]=var(pm, n, pm.ccnd, :pg, i)
            qg["$(n)_$(i)"]=var(pm, n, pm.ccnd, :qg, i)
        end
        constraint_model_voltage(pm, nw=n)

        for i in ids(pm, :ref_buses, nw=n)
            constraint_theta_ref(pm, i, nw=n)
        end

        for i in ids(pm, :bus, nw=n)
            constraint_power_balance_shunt(pm, i, nw=n)
        end

        for i in ids(pm, :branch, nw=n)
            constraint_ohms_yt_from(pm, i, nw=n)
            constraint_ohms_yt_to(pm, i, nw=n)

            constraint_voltage_angle_difference(pm, i, nw=n)

            constraint_thermal_limit_from(pm, i, nw=n)
            constraint_thermal_limit_to(pm, i, nw=n)
        end

        for i in ids(pm, :dcline, nw=n)
            constraint_dcline(pm, i, nw=n)
        end
        for i in ids(pm, :gen, nw=n)
            if pcon!=n
                JuMP.@constraint(pm.model, pg["$(pcon)_$(i)"]==pg["$(n)_$(i)"])
                JuMP.@constraint(pm.model, qg["$(pcon)_$(i)"]==qg["$(n)_$(i)"])    #Additional equality constraint for reactive power
            end
        end
    end

    objective_min_fuel_and_flow_cost(pm)
end

"""Corrective shceduling SCOPF
ONLY FOR DC FRAMEWORK
Need to specify the allowed correction rate 'rate'"""
function post_mndc_cscopf(pm::GenericPowerModel)
    pg=Dict()
    pcon=length(nws(pm))  #'pcon' corresponds to the pre-contingency case
    for (n, network) in nws(pm)

        variable_voltage(pm, nw=n)
        variable_generation(pm, nw=n)
        variable_branch_flow(pm, nw=n)
        variable_dcline_flow(pm, nw=n)

        for i in ids(pm, :gen, nw=n)
            pg["$(n)_$(i)"]=var(pm, n, pm.ccnd, :pg, i)
        end

        constraint_model_voltage(pm, nw=n)

        for i in ids(pm, :ref_buses, nw=n)
            constraint_theta_ref(pm, i, nw=n)
        end

        for i in ids(pm, :bus, nw=n)
            constraint_power_balance_shunt(pm, i, nw=n)
        end

        for i in ids(pm, :branch, nw=n)
            constraint_ohms_yt_from(pm, i, nw=n)
            constraint_ohms_yt_to(pm, i, nw=n)

            constraint_voltage_angle_difference(pm, i, nw=n)

            constraint_thermal_limit_from(pm, i, nw=n)
            constraint_thermal_limit_to(pm, i, nw=n)
        end

        for i in ids(pm, :dcline, nw=n)
            constraint_dcline(pm, i, nw=n)
        end
    end
    for (n,network) in nws(pm)
        if pcon!=n
            for i in ids(pm, :gen, nw=n)
                JuMP.@constraint(pm.model, pg["$(pcon)_$(i)"]>=(1-rate)pg["$(n)_$(i)"])   #Here, differences of (1-rate) of the value are tolerated
                JuMP.@constraint(pm.model, (1-rate)pg["$(pcon)_$(i)"]<=pg["$(n)_$(i)"])   # in generation levels between post contingency cases and the pcon problem
            end
        end
    end

    objective_min_fuel_and_flow_cost(pm)
end

"""Corrective shceduling SCOPF
ONLY FOR AC FRAMEWORK
Need to specify the allowed correction rate 'rate'"""
function post_mnac_cscopf(pm::GenericPowerModel)
    pg,qg=Dict(),Dict()
    pcon=length(nws(pm))
    for (n, network) in nws(pm)

        variable_voltage(pm, nw=n)
        variable_generation(pm, nw=n)
        variable_branch_flow(pm, nw=n)
        variable_dcline_flow(pm, nw=n)

        for i in ids(pm, :gen, nw=n)
            pg["$(n)_$(i)"]=var(pm, n, pm.ccnd, :pg, i)
            qg["$(n)_$(i)"]=var(pm, n, pm.ccnd, :qg, i) #qg is the  reactive  power
        end

        constraint_model_voltage(pm, nw=n)

        for i in ids(pm, :ref_buses, nw=n)
            constraint_theta_ref(pm, i, nw=n)
        end

        for i in ids(pm, :bus, nw=n)
            constraint_power_balance_shunt(pm, i, nw=n)
        end

        for i in ids(pm, :branch, nw=n)
            constraint_ohms_yt_from(pm, i, nw=n)
            constraint_ohms_yt_to(pm, i, nw=n)
            constraint_voltage_angle_difference(pm, i, nw=n)
            constraint_thermal_limit_from(pm, i, nw=n)
            constraint_thermal_limit_to(pm, i, nw=n)
        end

        for i in ids(pm, :dcline, nw=n)
            constraint_dcline(pm, i, nw=n)
        end
    end
    for (n,network) in nws(pm)              #Both  equality  constraint (real  and
        if pcon!=n                          #reactive  power) are  replaced  with  two
            for i in ids(pm, :gen, nw=n)    #looser  inequalities
                JuMP.@constraint(pm.model,pg["$(pcon)_$(i)"]>=(1-rate)pg["$(n)_$(i)"])
                JuMP.@constraint(pm.model,(1-rate)pg["$(pcon)_$(i)"]<=pg["$(n)_$(i)"])
                JuMP.@constraint(pm.model,qg["$(pcon)_$(i)"]>=(1-rate)qg["$(n)_$(i)"])
                JuMP.@constraint(pm.model,(1-rate)qg["$(pcon)_$(i)"]<=qg["$(n)_$(i)"])
            end
        end
    end
    objective_min_fuel_and_flow_cost(pm)
end

"""Relaxed Scopf: find a feasible solution
ONLY FOR DC FRAMEWORK"""
function post_mndc_relscopf(pm::GenericPowerModel)
    pg,sl=Dict(),Dict() #Initialisation for generation and slack variables
    pcon=length(nws(pm))
    for (n, network) in nws(pm)

        variable_voltage(pm, nw=n)
        variable_generation(pm, nw=n)
        variable_branch_flow(pm, nw=n)
        variable_dcline_flow(pm, nw=n)

        for i in ids(pm, :gen, nw=n)
            pg["$(n)_$(i)"]=var(pm, n, pm.ccnd, :pg, i)
        end

        constraint_model_voltage(pm, nw=n)

        for i in ids(pm, :ref_buses, nw=n)
            constraint_theta_ref(pm, i, nw=n)
        end

        for i in ids(pm, :bus, nw=n)
            constraint_power_balance_shunt(pm, i, nw=n)
        end

        for i in ids(pm, :branch, nw=n)
            constraint_ohms_yt_from(pm, i, nw=n)
            constraint_ohms_yt_to(pm, i, nw=n)
            constraint_voltage_angle_difference(pm, i, nw=n)
            constraint_thermal_limit_from(pm, i, nw=n)
            constraint_thermal_limit_to(pm, i, nw=n)
        end

        for i in ids(pm, :dcline, nw=n)
            constraint_dcline(pm, i, nw=n)
        end

        var(pm, n, pm.ccnd)[:sl] = JuMP.@variable(pm.model,     #Creation of a slack
        [i in ids(pm, :gen, nw=n)], base_name="$(n)_$(i)_sl")   #variable to model
        for i in ids(pm, :gen, nw=n)                            #absolute value in
            sl["$(n)_$(i)"]=var(pm, n, pm.ccnd, :sl, i)         #the objective
        end
    end
    for (n,network) in nws(pm)          #Constraints for the slack: greater than
        for i in ids(pm, :gen, nw=n)    #the distance between the generation levels
            JuMP.@constraint(pm.model, sl["$(n)_$(i)"]>=pg["$(pcon)_$(i)"]-pg["$(n)_$(i)"])
            JuMP.@constraint(pm.model, sl["$(n)_$(i)"]>=-pg["$(pcon)_$(i)"]+pg["$(n)_$(i)"])
        end
    end                                                         #The objective is
    JuMP.@objective(pm.model, Min,                              #just the L1 norm
        sum(sum(sl["$(n)_$(i)"] for i in ids(pm, :gen, nw=n))   #of the differences
        for  (n,network) in nws(pm)))                           #between power
end                                                             #generation levels

"""Relaxed Scopf: find a feasible solution
ONLY FOR AC FRAMEWORK"""
function post_mnac_relscopf(pm::GenericPowerModel)
    pg,qg=Dict(),Dict()
    sl,sq=Dict(),Dict()
    pcon=length(nws(pm))
    for (n, network) in nws(pm)

        variable_voltage(pm, nw=n)
        variable_generation(pm, nw=n)
        variable_branch_flow(pm, nw=n)
        variable_dcline_flow(pm, nw=n)

        for i in ids(pm, :gen, nw=n)
            pg["$(n)_$(i)"]=var(pm, n, pm.ccnd, :pg, i)
            qg["$(n)_$(i)"]=var(pm, n, pm.ccnd, :qg, i)
        end

        constraint_model_voltage(pm, nw=n)

        for i in ids(pm, :ref_buses, nw=n)
            constraint_theta_ref(pm, i, nw=n)
        end

        for i in ids(pm, :bus, nw=n)
            constraint_power_balance_shunt(pm, i, nw=n)
        end

        for i in ids(pm, :branch, nw=n)
            constraint_ohms_yt_from(pm, i, nw=n)
            constraint_ohms_yt_to(pm, i, nw=n)

            constraint_voltage_angle_difference(pm, i, nw=n)

            constraint_thermal_limit_from(pm, i, nw=n)
            constraint_thermal_limit_to(pm, i, nw=n)
        end

        for i in ids(pm, :dcline, nw=n)
            constraint_dcline(pm, i, nw=n)
        end

        var(pm, n, pm.ccnd)[:sl] = JuMP.@variable(pm.model,
        [i in ids(pm, :gen, nw=n)], base_name="$(n)_$(i)_sl")
        var(pm, n, pm.ccnd)[:sq] = JuMP.@variable(pm.model,
        [i in ids(pm, :gen, nw=n)], base_name="$(n)_$(i)_sq")
        for i in ids(pm, :gen, nw=n)
            sl["$(n)_$(i)"]=var(pm, n, pm.ccnd, :sl, i)
        end
        for i in ids(pm, :gen, nw=n)
            sq["$(n)_$(i)"]=var(pm, n, pm.ccnd, :sq, i)
        end
    end
    for (n,network) in nws(pm)
        for i in ids(pm, :gen, nw=n)
                JuMP.@constraint(pm.model, sl["$(n)_$(i)"]>=pg["$(pcon)_$(i)"]-pg["$(n)_$(i)"])
                JuMP.@constraint(pm.model, sl["$(n)_$(i)"]>=-pg["$(pcon)_$(i)"]+pg["$(n)_$(i)"])
                JuMP.@constraint(pm.model, sq["$(n)_$(i)"]>=qg["$(pcon)_$(i)"]-qg["$(n)_$(i)"])
                JuMP.@constraint(pm.model, sq["$(n)_$(i)"]>=-qg["$(pcon)_$(i)"]+qg["$(n)_$(i)"])
        end
    end

    JuMP.@objective(pm.model, Min,
        sum(
            sum(sl["$(n)_$(i)"]+sq["$(n)_$(i)"] for i in ids(pm, :gen, nw=n))
        for  (n,network) in nws(pm))
    )
end

"""Build a model from a single network .m file
Contingencies: all lines
Model: DC
Using scopf"""
function build_dc_scopf(file)
    network_data=parse_file("data/$(file)")             #Read file
    n=length(network_data["branch"])                    #Number of contingencies
    scall_network=replicate(network_data, n+1)          #Replicate network
    for key in keys(scall_network["nw"])
        if haskey(scall_network["nw"][key]["branch"], key)      #pcon does not
            delete!(scall_network["nw"][key]["branch"], key)    #remove line
        end
    end
    rpm = build_model(scall_network, DCPPowerModel, post_mndc_scopf, multinetwork=true)
    return rpm
end

"""Build a model from a single network .m file
Contingencies: all lines
Model: AC
Using scopf"""
function build_ac_scopf(file)
    network_data=parse_file("data/$(file)")
    n=length(network_data["branch"])
    scall_network=replicate(network_data, n+1) #All possible contingencies
    for key in keys(scall_network["nw"])
        if haskey(scall_network["nw"][key]["branch"], key)
            delete!(scall_network["nw"][key]["branch"], key)
        end
    end
    rpm = build_model(scall_network, ACPPowerModel, post_mnac_scopf, multinetwork=true)
    return rpm
end

"""Build a model from a single network .m file
Contingencies: all lines
Model: DC
Using relscopf"""
function build_dc_rel(file)
    network_data=parse_file("data/$(file)")
    n=length(network_data["branch"])
    scall_network=replicate(network_data, n+1) #All possible contingencies
    for key in keys(scall_network["nw"])
        if haskey(scall_network["nw"][key]["branch"], key)
            delete!(scall_network["nw"][key]["branch"], key)
        end
    end
    rpm = build_model(scall_network, DCPPowerModel, post_mndc_relscopf, multinetwork=true)
    return rpm
end

"""Build a model from a single network .m file
Contingencies: all lines
Model: AC
Using relscopf"""
function build_ac_rel(file)
    network_data=parse_file("data/$(file)")
    n=length(network_data["branch"])
    scall_network=replicate(network_data, n+1) #All possible contingencies
    for key in keys(scall_network["nw"])
        if haskey(scall_network["nw"][key]["branch"], key)
            delete!(scall_network["nw"][key]["branch"], key)
        end
    end
    rpm = build_model(scall_network, ACPPowerModel, post_mnac_relscopf, multinetwork=true)
    return rpm
end

"""Build a model from a single network .m file
Contingencies: all lines
Model: DC
Using cscopf"""
function build_dc_cor(file)
    network_data=parse_file("data/$(file)")
    n=length(network_data["branch"])
    scall_network=replicate(network_data, n+1) #All possible contingencies
    for key in keys(scall_network["nw"])
        if haskey(scall_network["nw"][key]["branch"], key)
            delete!(scall_network["nw"][key]["branch"], key)
        end
    end
    rpm = build_model(scall_network, DCPPowerModel, post_mndc_cscopf, multinetwork=true)
    return rpm
end

"""Build a model from a single network .m file
Contingencies: all lines
Model: AC
Using cscopf"""
function build_ac_cor(file)
    network_data=parse_file("data/$(file)")
    n=length(network_data["branch"])
    scall_network=replicate(network_data, n+1) #All possible contingencies
    for key in keys(scall_network["nw"])
        if haskey(scall_network["nw"][key]["branch"], key)
            delete!(scall_network["nw"][key]["branch"], key)
        end
    end
    rpm = build_model(scall_network, ACPPowerModel, post_mnac_cscopf, multinetwork=true)
    return rpm
end

"""Build a model from a single network .m file
Contingencies: cont
Model: DC
Using cscopf"""
function build_dc_cor(file,cont)
    network_data=parse_file("data/$(file)")
    n=length(cont)
    scc_network=replicate(network_data, n+1)
    for k in 1:n
        delete!(scc_network["nw"]["$(k)"]["branch"], cont[k])
    end
    rpm = build_model(scc_network, DCPPowerModel, post_mndc_cscopf, multinetwork=true)
    return rpm
end

"""Build a model from a single network .m file
Contingencies: cont
Model: AC
Using cscopf"""
function build_ac_cor(file,cont)
    network_data=parse_file("data/$(file)")
    n=length(cont)
    scc_network=replicate(network_data, n+1) 
    for k in 1:n
        delete!(scc_network["nw"]["$(k)"]["branch"], cont[k])
    end
    rpm = build_model(scc_network, ACPPowerModel, post_mnac_cscopf, multinetwork=true)
    return rpm
end
